<div align="center">
    <img src="https://codeberg.org/sympathy/sympathy/raw/branch/main/data/icons/hicolor/scalable/apps/page.codeberg.sympathy.Sympathy.svg" alt="The Sympathy logo">
    <h1>Sympathy</h1>
    Chat with your friends
</div>

## Why?

Sympathy is a new kind on messaging app, which intends to mimic the look and feel of old instant messaging apps, specifically, Empathy.

