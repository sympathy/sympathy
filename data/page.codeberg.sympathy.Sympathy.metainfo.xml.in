<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop">
  <id>page.codeberg.sympathy.Sympathy</id>
  <launchable type="desktop-id">page.codeberg.sympathy.Sympathy.desktop</launchable>
  <name>Sympathy</name>
  <developer_name>The Sympathy Project</developer_name>
  <project_license>GPL-3.0-or-later</project_license>
  <metadata_license>CC-BY-SA-3.0</metadata_license>
  <summary>Chat with your friends</summary>
  <content_rating type="oars-1.1">
    <content_attribute id="social-chat">intense</content_attribute>
  </content_rating>
  <update_contact>mdwalters.pm@proton.me</update_contact>
  <description>
    <p>Sympathy is a new kind on messaging app, which intends to mimic the look and feel of old instant messaging apps, specifically, Empathy.</p>
  </description>
  <requires>
    <internet>always</internet>
  </requires>
  <branding>
    <color type="primary" scheme_preference="light">#3584e4</color>
    <color type="primary" scheme_preference="dark">#8ff0a4</color>
  </branding>
  <url type="homepage">https://sympathy.codeberg.page/</url>
  <url type="bugtracker">https://codeberg.org/sympathy/sympathy/issues</url>
  <url type="translate">https://codeberg.org/sympathy/sympathy/src/branch/main/po</url>
  <url type="vcs-browser">https://codeberg.org/sympathy/sympathy</url>
  <screenshots>
    <screenshot type="default">
      <image type="source">https://codeberg.org/sympathy/sympathy/raw/branch/main/data/screenshots/Conversation%20View%20with%20Sidebar%20Open.png</image>
    </screenshot>
	  <screenshot>
      <image type="source">https://codeberg.org/sympathy/sympathy/raw/branch/main/data/screenshots/Conversation%20View%20with%20Sidebar%20Closed.png</image>
    </screenshot>
	  <screenshot>
      <image type="source">https://codeberg.org/sympathy/sympathy/raw/branch/main/data/screenshots/Conversation%20View%20with%20Sidebar.png</image>
    </screenshot>
  </screenshots>
  <releases>
    <release version="2024.01.01" date="2024-01-01">
      <description>
        <ul>
          <li>Ability to hide sidebar!</li>
          <li>Port to Python!</li>
          <li>Adds keyboard shortcut for the preferences window</li>
          <li>Adds date indicator</li>
          <li>Adds support for phones</li>
          <li>Initial message design</li>
          <li>Redesigns the status popover</li>
          <li>Adds attachments button</li>
          <li>Adds user's status as a subtitle to conversation pane</li>
          <li>Use Gtk.Image instead of Adw.ActionRow::icon-name, which is now deprecated</li>
        </ul>
      </description>
    </release>
    <release version="2023.12.25" date="2023-12-25">
      <description>
        <ul>
          <li>Port to Python!</li>
          <li>Adds keyboard shortcut for the preferences window</li>
          <li>Adds date indicator</li>
        </ul>
      </description>
    </release>
    <release version="2023.12.17" date="2023-12-17">
      <description>
        <ul>
          <li>Adds support for phones</li>
          <li>Initial message design</li>
          <li>Redesigns the status popover</li>
          <li>Adds attachments button</li>
          <li>Adds user's status as a subtitle to conversation pane</li>
        </ul>
      </description>
    </release>
    <release version="2023.12.10" date="2023-12-10">
      <description>
        <ul>
          <li>Use Gtk.Image instead of Adw.ActionRow::icon-name, which is now deprecated</li>
        </ul>
      </description>
    </release>
      <release version="2023.12.01" date="2023-12-01">
        <description>
          <ul>
            <li>First release! 🎉</li>
          </ul>
        </description>
      </release>
  </releases>
</component>
